from riddle_preprocessing.various_utils import *
from riddle_preprocessing.directories import *
import os

riddles_file='../riddle_generation/riddles.txt'
if not os.path.exists(riddles_dir): os.makedirs(riddles_dir)

with open(riddles_file,'r') as fp:
    riddles_list=fp.read().splitlines()
for riddle_str in riddles_list:
    xnsb=ridstr_2nsbfeat_invtoken(riddle_str)
    xsb=ridstr_2sbfeat_invtoken(riddle_str)
    x=np.concatenate([xnsb,xsb])
    riddle_id=extract_riddle_id(riddle_str)
    np.save(riddles_dir + 'riddle_' + riddle_id + '.npy', x)

tmp=7


