from riddle_preprocessing.various_utils import *
from riddle_preprocessing.misc_dbg import *
from riddle_preprocessing.riddles import *
from riddle_preprocessing.directories import *
#import json
import os
rid_str='new Riddle(1099,[1,0,0,1,1,1,1,1,0,1,1,1,0,1,1,1,0,1,1,0,0,0,0],2,["6","-","5","1"]),'
quest_str=ridstr_2queststr(rid_str)
sol_bin=ridstr_2solbin(rid_str)
quest_bin=ridstr_2questbin(rid_str)
print('solution rveng: ')
print (expbin_2explist(sol_bin))
feat_bin = strlist_2list(quest_str)
quest_str_rveng=vec_2str(feat_bin)
print('quest == quest_rveng? ')
print(quest_str_rveng==quest_str)
val_tk= get_val_tokens(quest_bin)
uch_tk= get_changed_tokens(quest_bin,sol_bin)

print('valid tokens: ')
print(val_tk)
print('tokens remain constant:')
print(uch_tk)

tmp=7