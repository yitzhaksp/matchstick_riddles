from riddle_preprocessing.various_utils import *
from riddle_preprocessing.riddles import *
from riddle_preprocessing.directories import *
#import json
import os

if not os.path.exists(riddles_dir):
    os.mkdir(riddles_dir)

sb=False #solution-based features
nsb_opt={'type':'tokens'}
sb_opt={}
if sb:
    df=pd.DataFrame()

for key in riddles_dict:
    if sb: #solution-based features
        x=get_sb_riddle_features(key,sb_opt)
        df=df.append(x)
        cols=list(df.columns)
    else:
        x=get_nsb_riddle_features(key,nsb_opt)
        np.save(riddles_dir+'riddle_'+str(key)+'.npy',x)
if sb:
    #df['id']=df.index
    df=df[sb_col_ind] #rearrange columns
    df.to_csv('sb_riddles_feat.csv',index=True,index_label='id')
