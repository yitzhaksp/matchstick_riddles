from riddle_preprocessing.various_utils import *
from riddle_preprocessing.misc_dbg import *
from riddle_preprocessing.riddles import *
from riddle_preprocessing.directories import *
#import json
import os
riddles_file='../riddle_generation/riddles.txt'
if not os.path.exists(riddles_dir): os.makedirs(riddles_dir)

with open(riddles_file,'r') as fp:
    riddles_list=fp.read().splitlines()
riddle_groups=riddles_2groups(riddles_list)
records=pd.read_csv('../solution_records/records_consolidated_23feb.csv')
records.dropna(inplace=True)
records.set_index('id',inplace=True,drop=True)
mads=[]
for i,riddle_group in enumerate(riddle_groups):
    print(i)
    a=[]
    for riddle_id in riddle_group:
        if int(riddle_id) in records.index:
            a.append(records.at[int(riddle_id),'average_time'])
    if len(a)>=2:
        a=np.array(a)
        mad=np.mean(np.abs(a-a.mean()))
        mads.append(mad)
print('mads_mean = {}'.format(np.mean(mads)))
print('mads_min = {}'.format(np.min(mads)))
print('mads_max = {}'.format(np.max(mads)))

tmp=8


