from riddle_preprocessing.riddles import *
from riddle_preprocessing.misc_dbg import *
from riddle_generation.tokens_and_transforms import *
from riddle_generation.tokens_binvec import *

import numpy as np
import pandas as pd
safety_eps=0.1
sb_col_ind = ['num_moves','op_const', 'rs_const', '2_digs_const', 'movs_inside_digs']

def ridstr_2nsbfeat_invtoken(riddle_str):
    feat_list=[]
    questbin=ridstr_2questbin(riddle_str)
    #feat_list.append(questbin)
    feat_list.append(get_val_tokens(questbin))
    feat_list.append(np.array( [extract_nummoves(riddle_str)]))
    return np.concatenate(  feat_list )

def ridstr_2sbfeat_invtoken(riddle_str):
    feat_list=[]
    questbin=ridstr_2questbin(riddle_str)
    solbin=ridstr_2solbin(riddle_str)
    feat_list.append(get_unchanged_tokens(questbin,solbin))
    feat_list.append(questbin.astype(int)-solbin.astype(int))
    #feat_list.append(  np.array([maxnumchanges_in_token(riddle_str,False)]) )
    return np.concatenate(  feat_list )

def maxnumchanges_in_token(riddle_str,absflg):
    quest = ridstr_2questbin(riddle_str)
    sol = ridstr_2solbin(riddle_str)
    qtokens=exp_2tokens(quest)
    stokens=exp_2tokens(sol)
    maxx=0
    for qtk,stk in zip(qtokens,stokens):
        numchng=np.sum(qtk!=stk)
        if absflg:
            maxx=max(maxx,numchng)
        else:
            if qtk.sum()==0:
                maxx=1
            else:
                maxx=max(maxx,numchng/qtk.sum())
    return round(maxx,2)

def exp_2tokens(exp):
    assert(exp.size==23)
    tokens=[]
    tokens.append(exp[:7])
    tokens.append(exp[7:9])
    tokens.append(exp[9:16])
    tokens.append(exp[16:])
    return tokens


def riddles_2groups(riddles_list):
    hlfeats = {}
    hlfeats_list = []
    for riddle_str in riddles_list:
        riddle_id = extract_riddle_id(riddle_str)
        hlfeats[riddle_id] = vec_2str(highlev_feat_invtoken(riddle_str))
        hlfeats_list.append(vec_2str(highlev_feat_invtoken(riddle_str)))
    hlfeats_unq = list(set(hlfeats_list))
    riddle_groups = []
    for x in hlfeats_unq:
        riddle_group = []
        for key in hlfeats:
            riddle_id = key
            if hlfeats[riddle_id] == x:
                riddle_group.append(riddle_id)
        riddle_groups.append(riddle_group)
    return riddle_groups

def get_unchanged_tokens(questbin,solbin):
    assert(questbin.size==23)
    assert(solbin.size==23)
    tkn1_eq=np.allclose(questbin[:7],solbin[:7])
    tkn2_eq=np.allclose(questbin[7:9],solbin[7:9])
    tkn3_eq=np.allclose(questbin[9:16],solbin[9:16])
    tkn4_eq=np.allclose(questbin[16:],solbin[16:])
    return [tkn1_eq,tkn2_eq,tkn3_eq,tkn4_eq]

def expbin_2explist(exp_bin):
    dig1 = digbin_2dig( exp_bin[:7])
    op = opbin_2op(exp_bin[7:9])
    dig2 = digbin_2dig(exp_bin[9:16])
    dig3 = digbin_2dig(exp_bin[16:])
    return [dig1, op, dig2, dig3]

def ridstr_2queststr(rid_str):
    a = rid_str.split('[')
    b = '[' + a[1]
    c = b.split(']')
    quest_str = c[0] + ']'
    return quest_str

def ridstr_2questbin(rid_str):
    feat_bin_str = ridstr_2queststr(rid_str)
    feat_bin = strlist_2list(feat_bin_str)
    return np.array(feat_bin,dtype=bool)


def ridstr_2solbin(rid_str):
    sol=get_solution_list(rid_str)
    dig1_bin=digit_binvecs[sol[0]]
    op_bin=operator_binvecs[sol[1]]
    dig2_bin=digit_binvecs[sol[2]]
    dig3_bin=digit_binvecs[sol[3]]
    return np.concatenate([dig1_bin,op_bin,dig2_bin,dig3_bin])


def get_solution_list(rid_str):
    a=rid_str.split('[')
    a=a[-1]
    b=a.split(']')
    b=b[0]
    c=b.split(',')
    return [int(c[0][1:-1]),c[1][1:-1],int(c[2][1:-1]),int(c[3][1:-1])]
# number of valid digits in riddle (0,1,2 or 3)

def get_val_tokens(exp_bin):
    assert(exp_bin.size==23)
    dig1=exp_bin[:7]
    op=exp_bin[7:9]
    dig2=exp_bin[9:16]
    dig3=exp_bin[16:]

    return np.array([is_digit(dig1),is_op(op),is_digit(dig2),is_digit(dig3)],dtype=bool)

def is_digit(token_bin):
    assert(token_bin.size==7)
    for key in digit_binvecs:
        if np.allclose(digit_binvecs[key],token_bin):
            return True
    tmp=7
    return False

def digbin_2dig(dig_bin):
    assert(dig_bin.size==7)
    for key in digit_binvecs:
        if np.allclose(digit_binvecs[key],dig_bin):
            return key
    tmp=7
    return 'no_dig'

def is_op(token_bin):
    assert(token_bin.size==2)
    for key in operator_binvecs:
        if np.allclose(operator_binvecs[key],token_bin):
            return True
    tmp=7
    return False

def opbin_2op(op_bin):
    assert(op_bin.size==2)
    for key in operator_binvecs:
        if np.allclose(operator_binvecs[key],op_bin):
            return key
    tmp=7
    return 'no_op'

def extract_riddle_id(riddle_str):
    a=riddle_str.split('le(')
    b=a[1].split(',')
    c=b[0]
    return c

def extract_nummoves(rid_str):
    a = rid_str.split('[')
    b = '[' + a[1]
    c = b.split(']')
    d = c[1]
    num_moves = int(d[1])
    return num_moves

def strlist_2list(in_str):
    b = in_str.split(',')
    b[0] = b[0][1]
    b[-1] = b[-1][0]
    out_list=[]
    for x in b:
        out_list.append(int(x))
    return out_list

def highlev_feat_invtoken(riddle_str):
    questbin = ridstr_2questbin(riddle_str)
    solbin=ridstr_2solbin(riddle_str)
    val_tokens = get_val_tokens(questbin)
    num_moves = extract_nummoves(riddle_str)
    unchanged_tokens=get_unchanged_tokens(questbin,solbin)
    return np.concatenate([np.array([num_moves]),val_tokens,unchanged_tokens])



def reformat_riddle_string(id,exp1list,nummoves,exp2list):
    with open('tmp1.txt','a') as fp:
        fp.write('\''+ str(id)+'\': Riddle('+str(id)+
                 ',Expression(['+exp1list[0] + ',\'' +exp1list[1]+'\','+exp1list[2] + ','+exp1list[3] + ']),'+
                 str(nummoves)+
                 ',Expression([' + exp2list[0] + ',\'' + exp2list[1] + '\',' + exp2list[2] + ',' + exp2list[3] + ']) ) \n'

                )


def is_movement_within_digit(a1,a2):
    within_digit=True
    d1,d2=min(a1,a2),max(a1,a2)
    if (d1!=d2) and (abs(token_transformations[str(d1)+str(d2)][1])> (0+safety_eps)):
        within_digit=False
    return within_digit


#do match movements take place only within the digits,
# or do we move a match from one digit to the other
def are_movements_within_digits(exp1,exp2):
    if not is_movement_within_digit(exp1.x,exp2.x): return False
    if not is_movement_within_digit(exp1.y,exp2.y): return False
    if not is_movement_within_digit(exp1.z,exp2.z): return False
    return True

#generate non-solution-based riddle features from riddles_dict
def get_nsb_riddle_features(id,opt):
    riddle=riddles_dict[id]
    tmp1 = riddle.to_be_solved
    if opt['type']=='tokens':
        feat=np.zeros(num_tokens+1,dtype=int)
        feat[:-1]=riddle.to_be_solved.compute_countform()
        feat[-1]=riddle.matches_to_move
    else:
        a=riddle.to_be_solved
        feat = np.concatenate((  a.compute_binform() , np.array([riddle.matches_to_move])   ))
    return feat

#generate solution-based riddle features from riddles_dict
def get_sb_riddle_features(id,opt):
    riddle = riddles_dict[id]
    op_const= (riddle.to_be_solved.op==riddle.solution.op)
    x_const= (riddle.to_be_solved.x==riddle.solution.x)
    y_const= (riddle.to_be_solved.y==riddle.solution.y)
    z_const= (riddle.to_be_solved.z==riddle.solution.z)
    x=pd.Series(len(sb_col_ind)*[None],index=sb_col_ind,name=id)
    x['num_moves']=riddle.matches_to_move
    x['op_const']=op_const
    x['rs_const']=z_const
    #tmp1=int(x_const)+int(y_const)+int(z_const)
    if (int(x_const)+int(y_const)+int(z_const) >= 2):
        x['2_digs_const']=True
    else:
        x['2_digs_const']=False
    x['movs_inside_digs']=are_movements_within_digits(riddle.to_be_solved,riddle.solution)
    return x



def expressions_diff(exp1,exp2):
    x_diff= (exp1.x!=exp2.x)
    op_diff= (exp1.op!=exp2.op)
    y_diff= (exp1.y!=exp2.y)
    z_diff= (exp1.z!=exp2.z)
    return [x_diff,op_diff,y_diff,z_diff]