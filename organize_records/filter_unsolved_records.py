import numpy as np
import pandas as pd
from format_solution_records.directories import *
from format_solution_records.various_utils import *

records=pd.read_csv(mturk_outp_dir+'all_records.csv')
records_solved=records[records['solved']==True]
records_solved.to_csv(mturk_outp_dir+'records_solved.csv',index=False)
tmp=1