import pandas as pd
import numpy as np
from format_solution_records.directories import *
from format_solution_records.various_utils import *
from funk_svd import SVD
from sklearn.metrics import mean_absolute_error
import os
import glob
#set working directory
os.chdir('../solution_records/records_old/records_until_10jan/')
outp_dir='../../records_10jan_1match/'
if not os.path.exists(outp_dir):
    os.makedirs(outp_dir)

#find all csv files in the folder
#use glob pattern matching -> extension = 'csv'
extension = 'csv'
all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
for f in all_filenames:
    df=pd.read_csv(f)
    df=df[df['id']<651]
    df.to_csv(outp_dir+f, index=False, encoding='utf-8-sig')

tmp=7