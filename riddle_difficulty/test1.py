import pandas as pd
from format_solution_records.directories import *
from riddle_difficulty.misc_utlis import *

records=pd.read_csv(mturk_outp_dir+'records_consolidated.csv')
records.dropna(subset=['time_med'],inplace=True)
ids=records['id'].to_list()
for i in range(1300):
    if not (i in ids):
        print(i)