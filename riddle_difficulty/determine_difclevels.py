import pandas as pd
from format_solution_records.directories import *
from riddle_difficulty.misc_utlis import *
import os
import json
difclev_names=['very easy','easy','medium','hard','very hard']
records=pd.read_csv(mturk_outp_dir+'records_consolidated_10mar_newid.csv')
records.dropna(subset=['time_med'],inplace=True)
records.set_index('id',inplace=True)
times=records['time_med']
bounds=[times.quantile(.2),times.quantile(.4),times.quantile(.6),times.quantile(.8)]
assert(len(bounds)==4) # we want to have 5 difclevels
difcs=categorize_times(times,bounds)
difcs_file='difcs1.txt'
difcs_json='difcs1.json'
if os.path.exists(difcs_file):
    os.remove(difcs_file)
with open(difcs_file, 'a') as fp:
    for key in difcs:
        fp.write('"'+str(key)+'":"'+str(difclev_names[difcs[key]])+'",\n')
with open(difcs_json, 'w') as fp:
    json.dump(difcs,fp)

tmp=7

