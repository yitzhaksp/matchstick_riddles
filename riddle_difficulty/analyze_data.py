import pandas as pd
from format_solution_records.directories import *
from riddle_difficulty.misc_utlis import *
import matplotlib.pyplot as plt

records=pd.read_csv(mturk_outp_dir+'records_consolidated.csv')
records.dropna(subset=['time_med'],inplace=True)
times=records['time_med']
q=.2
qnt=times.quantile(q)
print('quantile for {} is {}'.format(q,qnt))
plt.hist(times, bins=25)
plt.axvline(qnt, 0, 100, label='pyplot vertical line',color='g')
plt.show()
