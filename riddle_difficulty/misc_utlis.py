def categorize_times(times,bounds):
    difficulties={}
    prev=0
    for b in bounds:
        assert(b>prev)
        prev=b
    for ind,t in times.items():
        difc=len(bounds)
        for i,b in enumerate(bounds):
            if t<=b:
                difc=i
                break
        difficulties[ind]=difc
    return difficulties

