from riddle_generation.various_utils import *
from riddle_generation.Expression_utils import *
from riddle_generation.Riddle_utils import *
import os

num_moves=2
riddles_file='riddles_2moves1.txt'
if os.path.exists(riddles_file):
    os.remove(riddles_file)
true_expressions=all_true_expressions()
#exp=Expression((2,'+',3,5))
id=651
num_riddles_per_exp=6
for exp in true_expressions :
    a=generate_all_possible_riddles_fromexp_invtoken(exp,num_moves)
    if len(a)>num_riddles_per_exp:
        a=random.sample(a,num_riddles_per_exp)
    for to_be_solved in a:
        rid=Riddle('r'+str(id),to_be_solved,num_moves,exp,'invalid_token')
        with open(riddles_file,'a') as fp:
            rid.to_file(fp)
        id+=1
#changed_positions= (exp.compute_binform() != b)
tmp=7