from riddle_generation.Expression_utils import *
from riddle_generation.tokens_and_transforms import *
import random
import numpy as np
import itertools
import pandas as pd

def get_missing_ids(rec_cons_file,id_min,id_max):
    df=pd.read_csv(rec_cons_file)
    rid_present=df['id'].to_list()
    missing_ids=[]
    for id in range(id_min,id_max+1):
        if not (id in rid_present):
            missing_ids.append(id)
    return missing_ids

def increment_mulitind(ind,maxvalue):
    current_pos=len(ind)-1
    return increment_mulitind_inner(ind,maxvalue,current_pos)

def increment_mulitind_inner(ind,maxind,current_pos):
    if ind[current_pos]<maxind:
        ind[current_pos]+=1
        return ind
    else:
        if current_pos==0:
            return None
        else:
            ind[current_pos]=0
            return increment_mulitind_inner(ind,maxind,current_pos-1)
    tmp=7


# get positions of 'true' in binary vector
def get_positions_of_true(x):
    true_pos,false_pos=[],[]
    for i in range(x.size):
        if x[i]==True:
            true_pos.append(i)
        else:
            false_pos.append(i)
    return true_pos,false_pos



# matchvec is binary vec which indicates which positions are filled with matches
def innercomp_generate_random_riddle_fromexp_invtoken(mvec,num_moves):
    match_positions, nonmatch_positions=get_positions_of_true(mvec)
    if len(match_positions)<num_moves or len(nonmatch_positions)<num_moves:
        return None
    match_positions_flp=random.sample(match_positions,num_moves)
    nonmatch_positions_flp=random.sample(nonmatch_positions,num_moves)
    mvec_new=np.copy(mvec)
    for i in range(num_moves):
        mvec_new[match_positions_flp[i]]=False
        mvec_new[nonmatch_positions_flp[i]]=True
    return mvec_new

def generate_random_riddle_fromexp_invtoken(exp,num_moves):
    mvec=exp.compute_binform() #match vector
    to_be_solved=innercomp_generate_random_riddle_fromexp_invtoken(mvec,num_moves)
    return to_be_solved


def generate_all_possible_riddles_fromexp_invtoken(exp,num_moves):
    mvec=exp.compute_binform()
    match_positions, nonmatch_positions=get_positions_of_true(mvec)
    to_be_solved_all=[]
    for match_positions_flp in itertools.combinations(match_positions,num_moves):
        for nonmatch_positions_flp in itertools.combinations(nonmatch_positions, num_moves):
            a=np.copy(mvec)
            for i in range(num_moves):
                a[match_positions_flp[i]]=False
                a[nonmatch_positions_flp[i]]=True
            to_be_solved_all.append(a)
    return to_be_solved_all


    toind=num_moves*[0]





def fetch_expr_by_diff(expr1,diff):
    expr_list=[]
    for x in digits:
        for op in operators:
            for y in digits:
                for z in digits:
                    expr=Expression((x,op,y,z))
                    expr_trans = Expression_Transformation(expr, expr1)
                    expr_trans.compute_diff()
                    if (expr_trans.num_matches_required==diff['num_matches_required']) and (expr_trans.num_moves==diff['num_moves']):
                        expr_list.append(expr)
    return expr_list


def all_true_expressions():
    expr_list=[]
    for x in digits:
        for op in operators:
            for y in digits:
                for z in digits:
                    expr=Expression((x,op,y,z))
                    if expr.is_true():
                        expr_list.append(expr)

    return expr_list

