from riddle_generation.tokens_and_transforms import *
from riddle_generation.tokens_binvec import *

import operator

#ops = { "+": operator.add, "-": operator.sub, '*':operator.mul, '/':operator.truediv }
ops = { "+": operator.add, "-": operator.sub}

def get_token_transformation(token1, token2):
    token1_id, token2_id=  token_ids[token1], token_ids[token2]
    if token1_id == token2_id:
        trans_tuple =(0,0)
    elif token1_id < token2_id:
        ind = token1 + token2
        trans_tuple=token_transformations[ind]
    else:
        ind = token2 + token1
        tmp=token_transformations[ind]
        trans_tuple=(tmp[0],-tmp[1])
    return Token_Transformation(trans_tuple)


class Expression(object):
    def __init__(self, expr):
        self.x=expr[0]
        self.op = expr[1]
        self.y = expr[2]
        self.z = expr[3]
    def compute_binform(self):
            return np.concatenate((digit_binvecs[self.x],operator_binvecs[self.op],digit_binvecs[self.y],digit_binvecs[self.z]))
    def compute_countform(self):
        a = np.zeros(num_tokens, dtype=int)
        a[token_ids[self.x]] += 1
        a[token_ids[self.op]] += 1
        a[token_ids[self.y]] += 1
        a[token_ids[self.z]] += 1
        return a
    def print(self):
        print(self.x+self.op+self.y+'='+self.z)
    def to_string(self):
        return '["'+str(self.x)+'","'+str(self.op)+'","'+str(self.y)+'","'+str(self.z)+'"]'


    def valid_calculation(self):
        if (self.op=='/') and (self.y=='0'):
            return False
        else:
            return True
    def is_true(self):
        if not self.valid_calculation():
            return False
        else:
            right_side= ops[self.op](int(self.x),int(self.y))
            if int(self.z)==right_side:
                return True
            else:
                return False


class Token_Transformation(object):
    def __init__(self, trans_tuple):
        self.num_moves=trans_tuple[0]
        self.num_insertions=trans_tuple[1]

class Expression_Transformation(object):
    def __init__(self, expr1,expr2):
        self.trans_tuple=(get_token_transformation(expr1.x,expr2.x),
                      get_token_transformation(expr1.op, expr2.op),
                      get_token_transformation(expr1.y, expr2.y),
                      get_token_transformation(expr1.z, expr2.z),
                      )


    def compute_diff(self):
        num_moves_agg ,num_positive_insertions, num_deletions = 0, 0, 0
        for token_trans in self.trans_tuple:
            num_moves_agg+=token_trans.num_moves
            if token_trans.num_insertions >= 0:
                num_positive_insertions += token_trans.num_insertions
            else:
                num_deletions += abs(token_trans.num_insertions)
        self.num_moves=num_moves_agg+ min(num_positive_insertions, num_deletions)
        self.num_matches_required= num_positive_insertions - num_deletions
        #print('following operations are required for the transformation:')
        #print('num_moves={}, num_matches_required={}'.format(self.num_moves,self.num_matches_required))


