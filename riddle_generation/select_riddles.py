from riddle_generation.various_utils import *
from format_solution_records.directories import *
from riddle_generation.Expression_utils import *
import itertools
ind_missing=get_missing_ids(mturk_outp_dir+'records_consolidated_23feb.csv',651,1298)
ind_too_few=[
1080,
761,
608,
281,
1279,
821,
1247,
1027,
685,
762,
1267,
699,
1002,
1013,
634,
1205,
741,
1007

]
sel_ind=ind_missing+ind_too_few
with open('../riddle_generation/riddles.txt', 'r') as fp:
# read file into list
    content = fp.read().splitlines()
selected=[]
for riddle_str in content:
    a=riddle_str.split('e(')
    b=a[1].split(',[')
    riddle_ind=int(b[0])
    if riddle_ind in sel_ind:
        selected.append(riddle_str)

out_file_name='riddles_sel.txt'
with open(out_file_name, 'w') as fp:
    for riddle_str in selected:
        fp.write(riddle_str+'\n')
print(selected)

tmp=7