import numpy as np

class Riddle(object):
    def __init__(self,id,to_be_solved,matches_to_move,solution,token_type):
        self.id=id
        self.to_be_solved=to_be_solved
        self.matches_to_move=matches_to_move
        self.solution=solution
        self.token_type=token_type
    def to_binary(self):
        self.to_be_solved.to_binary()
        self.solution.to_binary()


    def to_file(self,fp):
        if self.token_type=='valid_token':
            fp.write('new Riddle('+str(self.id)+','+self.to_be_solved.to_string()+','+str(self.matches_to_move)+','+self.solution.to_string()+'), \n')
        elif self.token_type=='invalid_token':
            fp.write('new Riddle("'+str(self.id)+'",'+
                     np.array2string(self.to_be_solved.astype(int),separator=',')+
                     ',' + str(self.matches_to_move) + ',' + self.solution.to_string() + '), \n'
                     )
        else:
            print('cannot write to file. unknown token type')

