digits=[0,1,2,3,4,5,6,7,8,9]
#operators=['+','-','*','/']
operators=['+','-']
num_tokens=len(digits)+len(operators)

token_ids={
0:0,
1:1,
2:2,
3:3,
4:4,
5:5,
6:6,
7:7,
8:8,
9:9,
'+':10,
'-':11,
'*':12,
'/':13

}

token_transformations= {
'01': (0,-4),
'02': (1,-1),
'03': (1,-1),
'04': (1,-2),
'05': (1,-1),
'06': (1,0),
'07': (0,-3),
'08': (0,1),
'09': (1,0),
'12': (1,3),
'13': (0,3),
'14': (0,2),
'15': (1,3),
'16': (1,4),
'17': (0,1),
'18':  (0,5),
'19': (0,4),
'23': (1,0),
'24': (2,-1),
'25': (2,0),
'26': (1,1),
'27': (1,-2),
'28': (0,2),
'29': (1,1),
'34':(1,-1),
'35': (1,0),
'36': (1,1),
'37': (0,-2),
'38': (0,2),
'39': (0,1),
'45': (1,1),
'46': (1,2),
'47': (1,-1),
'48': (0,3),
'49':  (0,2),
'56': (0,1),
'57':(1,-2),
'58': (0,2),
'59': (0,1),
'67': (1,-3),
'68': (0,1),
'69': (1,0),
'78': (0,4),
'79': (0,3),
'89': (0,-1),
'+-': (0,-1),
'+*': (1,1),
'+/': (2,0),
'-*':(1,2),
'-/': (1,1),
'*/': (2,-1)
}