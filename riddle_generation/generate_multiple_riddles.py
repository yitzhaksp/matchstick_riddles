from utils.Expression_utils import *
from utils.Riddle_utils import *
from utils.token_transforms import *
from riddle_generation.various_utils import *
import random
true_expressions=all_true_expressions()
random.shuffle(true_expressions)
diff={'num_moves':2,
      'num_matches_required':0}
riddles=[]
riddle_id=110
for true_expr in true_expressions:
    trueexpr_circle_tmp=fetch_expr_by_diff(true_expr,diff)
    trueexpr_circle=[]
    for expr in trueexpr_circle_tmp:
        if not expr.is_true():
            trueexpr_circle.append(expr)
    if trueexpr_circle:
        riddles.append(Riddle(riddle_id,random.choice(trueexpr_circle),diff['num_moves'],true_expr))
        riddle_id+=1
suf=str(diff['num_moves'])+'moves.txt'
fp =open('riddles_'+suf, 'w')
for i,riddle in enumerate(riddles):
    #print('riddle nr. {}'.format(i))
    riddle.to_file(fp)
fp.close()



tmp=71