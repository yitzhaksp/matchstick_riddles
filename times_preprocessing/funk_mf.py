import pandas as pd
import numpy as np
from format_solution_records.directories import *
from format_solution_records.various_utils import *
from funk_svd import SVD_YS, SVD #SVD_YS prints mae after each iteration
from sklearn.metrics import mean_absolute_error

records=pd.read_csv(mturk_outp_dir+'all_records.csv')
X=records[records['solved']==True]
X=X[['id','worker_id','required_time']]
X.rename(columns={'id':'i_id','worker_id':'u_id','required_time':'rating'}, inplace=True)
X['rating']=X['rating'].clip(1,600)
X_train = X.sample(frac=0.8, random_state=7)
X_val = X.drop(X_train.index.tolist()).sample(frac=0.5, random_state=8)
X_test = X.drop(X_train.index.tolist()).drop(X_val.index.tolist())

u_ids=X['u_id'].to_list()
u_ids=list(set(u_ids)) # filter unique values
u_ids_head=u_ids[:3]
i_ids=X['i_id'].to_list()
i_ids=list(set(i_ids)) # filter unique values
df=pd.DataFrame(columns=i_ids)

svd = SVD_YS(learning_rate=0.001, regularization=0.005, n_epochs=300,
          n_factors=20, min_rating=1, max_rating=600)

svd.fit(X_train,X_val, early_stopping=True, shuffle=False)
pred = svd.predict(X)
mae = mean_absolute_error(X["rating"], pred)
print(f' Val MAE: {mae:.2f}')

fill_data=True
if fill_data:
    for count,u_id in enumerate(u_ids):
        print(count)
        row = pd.Series(len(i_ids)*[None], index=i_ids, name=u_id)
        for i_id in i_ids:
            row[i_id]=svd.predict_pair(u_id,i_id)
        df=df.append(row)

    print('computing averages over columns')
    means=pd.Series(df.shape[1]*[None], index=df.columns, name='time')
    means.index.name='riddle_id'
    for col_name in df:
        means[col_name]=round(df[col_name].mean())
    means.to_csv(mturk_outp_dir+'funksvd_means_es_20.csv',index=True)

tmp=7