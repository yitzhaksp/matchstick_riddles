import numpy as np
import pandas as pd
from format_solution_records.directories import *
from format_solution_records.various_utils import *

coffee_trh=1800
clip_trh=5000
num_sigmas=3.0
eps=0.1
#ignore_unsolved=True
records=pd.read_csv(mturk_outp_dir+'all_records.csv')
records_solved=records[records['solved']==True]
all_ids=list(dict.fromkeys(list(records.id)))
this_id_times=[]
columns_index=['num_entries','solved','after_outlier_filter','time_avg','time_med','time_2nd',]
records_consolidated=pd.DataFrame(columns=columns_index)
times_dict={}
solution_count,skip_count = {},{}
for id in all_ids:
    times_dict[id]=[]
    solution_count[id], skip_count[id] = 0, 0
    records_consolidated=records_consolidated.append(pd.Series([0,None,None,None,None,None],index=columns_index,name=id))
for ind,row in records.iterrows():
    records_consolidated.at[row.id,'num_entries'] += 1
for ind, row in records_solved.iterrows():
    solution_count[row.id]+=1
    times_dict[row.id].append(row.required_time)
sigma=compute_sigma(times_dict,coffee_trh)
for id in times_dict.keys():
    clean_times=remove_outliers(np.array(times_dict[id]),coffee_trh,clip_trh,sigma,num_sigmas)
    print('id={}, len={}'.format(id,clean_times.size))
    clean_times.sort()
    records_consolidated.at[id,'after_outlier_filter'] = len(clean_times)
    records_consolidated.at[id,'solved']= solution_count[id]
    #records_consolidated.at[id,'skip_ratio']=round(skip_count[id]/records_consolidated.at[id, 'num_entries'],2)
    records_consolidated.at[id,'time_avg']=round(np.mean(clean_times))
    records_consolidated.at[id,'time_med']=round(np.median(clean_times))
    records_consolidated.at[id,'time_2nd']=round(clean_times[1])

records_consolidated.index.name='id';
records_consolidated.to_csv(mturk_outp_dir+'records_consolidated.csv',index=True)
tmp=7