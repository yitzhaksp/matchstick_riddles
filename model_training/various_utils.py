from tensorflow import keras
from tensorflow.keras import layers
import pandas as pd
import numpy as np
import math

def load_data(records_file,riddles_dir,opt):
    records=pd.read_csv(records_file)
    records.set_index('id',inplace=True,drop=True)
    features,targets,ids=[],[],[]
    for id in records.index:
        if not math.isnan(records.at[id,opt['target_column']]): #rule out Nan
            a=np.load(riddles_dir+'riddle_'+str(id)+'.npy')
            features.append(a)
            targets.append(records.at[id,opt['target_column']])
            ids.append(id)
    return np.array(features), np.array(targets),np.array(ids,dtype=int)

def norm(x,mean,std):
  return (x - mean) / std

def build_model(input_dim):
  #neurons_per_layer=input_dim
  model = keras.Sequential([

    layers.Dense(10, activation='relu', input_shape=[input_dim]),
    #layers.Dropout(0.5),
    layers.Dense(10, activation='relu'),
     #layers.Dropout(0.5),
    layers.Dense(10, activation='relu'),
    #layers.Dropout(0.5),
    #layers.Dense(5, activation='relu'),

      layers.Dense(1)
  ])
  optimizer = keras.optimizers.RMSprop(0.001)
  model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae', 'mse'])
  return model

