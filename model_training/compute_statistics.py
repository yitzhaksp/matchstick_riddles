from model_training.various_utils import *
last1=650 # highest 1-move-riddle id
data_flg='simple_avg'
if data_flg=='simple_avg':
    records_file='../solution_records/records_consolidated_23feb.csv'
    records = pd.read_csv(records_file)
    records.set_index('id', inplace=True, drop=True)
    records1=records[records.index<=last1]
    records2=records[records.index>last1]
    m1=records1.average_time.mean()
    m2=records2.average_time.mean()
    mad1=records1.average_time.mad()
    mad2=records2.average_time.mad()
elif data_flg=='funksvd_avg':
    means_file='../solution_records/funksvd_means_es_20.csv'
    means = pd.read_csv(means_file)
    records1=means[means['riddle_id']<=last1]
    records2=means[means['riddle_id']>last1]
    m1 = records1.time.mean()
    m2 = records2.time.mean()
    mad1 = records1.time.mad()
    mad2 = records2.time.mad()

print('1-moves riddles: ')
print('amount: {0:.2f}'.format(records1.shape[0]))
#print('min time: {0:.2f}'.format(records1.average_time.min()))
#print('max: {0:.2f}'.format(records1.average_time.max()))
print('mean: {0:.2f}'.format(m1))
print('mad: {0:.2f}'.format(mad1))
print('2-moves riddles: ')
print('amount: {0:.2f}'.format(records2.shape[0]))
#print('min time: {0:.2f}'.format(records2.average_time.min()))
#print('max: {0:.2f}'.format(records2.average_time.max()))
print('mean : {0:.2f}'.format(m2))
print('mad: {0:.2f}'.format(mad2))

tmp=7