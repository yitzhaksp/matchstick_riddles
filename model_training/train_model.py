
from __future__ import absolute_import, division, print_function, unicode_literals
from model_training.various_utils import *
from sklearn.utils import shuffle
import seaborn
import pathlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling
from sklearn.model_selection import KFold


load_data_opt={'target_column':'time_med'}
train_ratio=0.6
val_ratio=0.2
n_split=5
EPOCHS = 11000
train_data_flg=1
X,Y,ids=load_data('../solution_records/records_consolidated.csv','../riddle_preprocessing/encoded_riddles/',load_data_opt)
print('number of features: {}'.format(X.shape[1]))
X, Y,ids = shuffle(X, Y, ids,random_state=0)
n_total=X.shape[0]
n_train=int(train_ratio*n_total)
n_val=int(val_ratio*n_total)

X_trv,Y_trv,ids_trv=X[:(n_train+n_val)],Y[:(n_train+n_val)],ids[:(n_train+n_val)]
X_test,Y_test,ids_test=X[(n_train+n_val):],Y[(n_train+n_val):],ids[(n_train+n_val):]
mae_prim_list,mae_DL_list=[],[]
for train_index,val_index in KFold(n_split).split(X_trv):
    a=7
    X_train, Y_train, ids_train = X_trv[train_index], Y_trv[train_index], ids_trv[train_index]
    X_val, Y_val, ids_val = X_trv[val_index], Y_trv[val_index], ids_trv[val_index]
    mean_train=np.mean(Y_train)
    mae_prim = np.mean(np.abs(Y_val - mean_train))
    mae_prim_list.append(mae_prim)
    print('mae_primitive={}'.format(mae_prim))
    model=build_model(X.shape[1])
    model.summary()
    example_batch = X_train[:10]
    example_result = model.predict(example_batch)
    print(example_result)
    plotter = tfdocs.plots.HistoryPlotter(smoothing_std=2)

    # The patience parameter is the amount of epochs to check for improvement
    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=30)
    history = model.fit(X_train, Y_train,
                        epochs=EPOCHS, validation_data=(X_val,Y_val), verbose=0,
                        callbacks=[early_stop,tfdocs.modeling.EpochDots()])
    model.save('my_model.h5')
    plotter.plot({'required time': history}, metric = "mae")
    #plt.ylim([0, 10])
    plt.ylabel('MAE')
    plt.show()
    loss, mae, mse = model.evaluate(X_val, Y_val, verbose=2)
    print("Validation set Mean Abs Error: {:5.2f}".format(mae))
    mae_DL_list.append(mae)
    Yhat_val = model.predict(X_val).flatten()
    error=Y_val - Yhat_val
    abs_error=abs(error)
    ids_val_sorted=np.flip(ids_val[abs_error.argsort()]) #highest error first
    np.save('highest_errors_ids1.npy',ids_val_sorted)
    np.save('highest_errors1.npy',np.flip(error[abs_error.argsort()])       )
    analyze_model=False
    if analyze_model:
        a = plt.axes(aspect='equal')
        plt.scatter(Y_val, Yhat_val)
        plt.xlabel('True Values')
        plt.ylabel('Predictions')
        ideal_x = [0, 250]
        ideal_y = ideal_x
        plt.xlim(ideal_x)
        plt.ylim(ideal_y)
        _ = plt.plot(ideal_x, ideal_y)
        plt.show()

        error = Y_val - Yhat_val
        plt.hist(error, bins = 25)
        plt.xlabel("Prediction Error")
        _ = plt.ylabel("Count")
        plt.show()

print('mae_prim_nsplit: {}'.format(  sum(mae_prim_list)/len(mae_prim_list)    ))
print('mae_DL_nsplit: {}'.format(  sum(mae_DL_list)/len(mae_DL_list)    ))

tmp=9

