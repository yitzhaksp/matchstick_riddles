
function Match(id,x,y,direction,filled_flg,selectable,fill_color=match_color){
    //alert('creating match '+id+ '; selectable='+selectable)
    this.id=id
    this.x=x
    this.y=y
    this.direction=direction
    this.selectable=selectable
    this.filled_flg=filled_flg
    this.fill_color=fill_color
    //alert('finished match '+this.id+ '; selectable='+this.selectable)
}

function get_possible_matches_for_digit(pos,filled_flg,selectable){
    possible_matches=[
	new	Match('a',topleft_x[pos-1]+match_width,topleft_y,'h',filled_flg,selectable),
	new  Match('b',topleft_x[pos-1]+match_length+match_width,topleft_y,'v',filled_flg,selectable),
	new Match('c',topleft_x[pos-1]+match_width+match_length,topleft_y+ match_length,'v',filled_flg,selectable),
	new Match('d',topleft_x[pos-1]+match_width,topleft_y+2*match_length-match_width,'h',filled_flg,selectable),
	new Match('e',topleft_x[pos-1],topleft_y+ match_length,'v',filled_flg,selectable),
	new Match('f',topleft_x[pos-1],topleft_y,'v',filled_flg,selectable),
	new Match('g',topleft_x[pos-1]+match_width,topleft_y+ match_length-0.5*match_width,'h',filled_flg,selectable),
	


]
    return possible_matches;
}

function get_possible_matches_for_operator(pos,filled_flg,selectable){
    possible_matches=[
	new Match('a',topleft_x[pos-1],topleft_y+ match_length,'h',filled_flg,selectable),
	new Match('b',topleft_x[pos-1]+0.4*match_length,topleft_y+ 0.6*match_length,'v',filled_flg,selectable),



]
    return possible_matches
}


function get_matches_for_equals(){
    var filled_flg=true
    var selectable=false
    var color=	'#778899'
    possible_matches=[
	new Match('eq1',topleft_x[3],topleft_y+ 0.8*match_length,'h',filled_flg,selectable,color),
	new Match('b',topleft_x[3],topleft_y+ 1.2*match_length,'h',filled_flg, selectable,color)

]
    return possible_matches
}

var digits_bin={
    '0': [1,1,1,1,1,1,0], // 0
    '1' :[0,1,1,0,0,0,0], // 1
    '2' :[1,1,0,1,1,0,1], // 2
    '3' :[1,1,1,1,0,0,1], //3
    '4' :[0,1,1,0,0,1,1], //4
    '5':[1,0,1,1,0,1,1], //5
    '6':[1,0,1,1,1,1,1], //6
    '7':[1,1,1,0,0,0,0], //7
    '8':[1,1,1,1,1,1,1], //8
    '9':[1,1,1,1,0,1,1], //9


}

var operators_bin={
    '+':[1,1],
    '-':[1,0]
}


function add_matches_to_collection(active_matches,possible_matches,shape_type,canv_state){
    for (i=0;i<possible_matches.length;i++){
	if (active_matches[i]==1){
            // alert('matchnr='+i+'; selectable='+possible_matches[i].selectable)
	    canv_state.addShape(create_match_shape(possible_matches[i]),shape_type); }
    }
}

function create_invexpr(invexpr_bin,canv_state){
    var possible_matches1=get_possible_matches_for_digit(1,true,true);
    var possible_matches2=get_possible_matches_for_operator(2,true,true);
    var possible_matches3=get_possible_matches_for_digit(3,true,true);
    var possible_matches4=get_possible_matches_for_digit(5,true,true);

    add_matches_to_collection(invexpr_bin.slice(0,7),possible_matches1,'Match',canv_state); 
    add_matches_to_collection(invexpr_bin.slice(7,9),possible_matches2,'Match',canv_state); 
    add_matches_to_collection(invexpr_bin.slice(9,16),possible_matches3,'Match',canv_state); 
    add_matches_to_collection(invexpr_bin.slice(16,23),possible_matches4,'Match',canv_state); 

}

function create_digit_background(pos,canv_state){
    var possible_matches=get_possible_matches_for_digit(pos,false,false)
    var  digit_bin=digits_bin['8']
    add_matches_to_collection(digit_bin,possible_matches,'Form',canv_state)    
}

function create_operator_background(canv_state){
    var possible_matches=get_possible_matches_for_operator(2,false,false)
    var  operator_bin=[1,1]
    add_matches_to_collection(operator_bin,possible_matches,'Form',canv_state)    
}

function create_digit(digit,pos,canv_state){
    var possible_matches=get_possible_matches_for_digit(pos,true,true)
    var  digit_bin=digits_bin[digit]
    add_matches_to_collection(digit_bin,possible_matches,'Match',canv_state)    
}

function create_operator(op,canv_state){
    var possible_matches=get_possible_matches_for_operator(2,true,true);
    var  operator_bin=operators_bin[op];
    add_matches_to_collection(operator_bin,possible_matches,'Match',canv_state);    
}

function create_equals(canv_state){
    var possible_matches=get_matches_for_equals()
    add_matches_to_collection([1,1],possible_matches,'Match',canv_state)    
}


function create_match_shape(match){
    if (match.direction=='v'){
	shape_width=match_width
	shape_height=match_length
    }
    else{
	shape_width=match_length
	shape_height=match_width
    }
    match_shape=new Shape(match.x,match.y,shape_width,shape_height,match.filled_flg,match.selectable,match.fill_color)

    return match_shape
}
