var current_worker_string=null;
var rd_buttons=['enj','perf','int','sim']

function survey_to_string(survey_items){
    var string='_$compl='+survey_is_completed(survey_items)
    for (var i = 0; i < survey_items.length; i++) {        
        string+='_$'+survey_items[i]+'='+rbtn_value(survey_items[i])
    }
    return string
}

function survey_is_completed(survey_items){
for (var i = 0; i < survey_items.length; i++) {
        if ( rbtn_is_checked(survey_items[i]) == false){
            return false
        }
    }
    return true
}

function rbtn_value(rbtn_name){
    var x='none'
    if (rbtn_is_checked(rbtn_name)){
        x=document.querySelector('input[name="'+rbtn_name+'"]:checked').value    
    }
    return x
}

function rbtn_is_checked(rbtn_name){
var radios = document.getElementsByName(rbtn_name);
for (var i = 0, length = radios.length; i < length; i++) {
  if (radios[i].checked) {
    return true;
  }
}
return false
}

function array_equals(x,y){
    for (i=0;i<x.length;i++){
	if (x[i]!=y[i]){
	    return false}
    }
    return true
}




function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function Worker(birthyear,gender,education){
    this.birthyear=birthyear;
    this.gender=gender;
    this.education=education;
}

function record_to_string(record){
    var string='_$id='+record.id+
	'_$solved='+record.solved+
    '_$match_moved='+record.match_moved+
    '_$required_time='+record.required_time;

    return string
}

function solution_records_to_string(solution_records){
    var prefix= "$num_correct_"+num_correct_solutions+"_"
    var solution_records_string=prefix+recarray_to_string(solution_records,record_to_string);
    return solution_records_string;
}

function worker_info_to_string(worker){
    var string='&_$birthyear='+worker.birthyear
	+ '_$gender='+worker.gender
	+'_$education='+worker.education;

    return string
}

function recarray_to_string(array,to_string_func){
    var string='';
    for (var i = 0; i < array.length; i++) {
	//console.log(to_string(array[i]))
	   string+='_&'+to_string_func(array[i])
    }
    return string;
}




function go_to_task() {
    var birthyear=document.getElementById("birthyear").value;
    var gender=document.getElementById("gender").value;
    var education=document.getElementById("education").value;
    var current_worker=new Worker(birthyear,gender,education);
    
    if ((birthyear=="empty") || (gender=="empty")|| (education=="empty")){
	alert("Please answer all questions");
	return ;
    }
   document.getElementById("worker_data_page").style.display="none"
    document.getElementById("task_page").style.display="block" //block previous display settings
    start_time= new Date();
    current_worker_string=worker_info_to_string(current_worker);
    if (mturk_flg==true){    
    document.getElementById('worker_data').value=current_worker_string
    }
    




}
