function check_tokens_valid(canv_state){
    var positions=[1,2,3,5];
    var token_types=['dig','op','dig','dig'];
    for (var i=0;i<4;i++){
       token=read_digit(positions[i],token_types[i],canv_state);
	   if (token==null){
	       alert('Expression is illegal. Token at position ' + positions[i]+ ' is illegal');
	       return false;}
    }
    return true;
};


var math_it_up = {
    '+': function (x, y) { return x + y; },
    '-': function (x, y) { return x - y ;}
}  


function check_expression_correct(canv_state){
    if (check_tokens_valid(canv_state)==true){
        expr=read_expression_on_display(canv_state)
        var left_side= math_it_up[expr[1]](parseInt(expr[0]),parseInt(expr[2]));
        var right_side=parseInt(expr[3]);
        return (left_side==right_side);
    }
    else {return false}


    
}
