function read_digit(pos,dig_op,canv_state){
    //alert('digop is '+dig_op)
    if (dig_op=='dig') {
	var token_bin=[0,0,0,0,0,0,0]
	var possible_matches=get_possible_matches_for_digit(pos,true,true)
	var known_tokens=digits_bin
    }
    if (dig_op=='op'){
	var token_bin=[0,0]
	var possible_matches=get_possible_matches_for_operator(pos,true,true)
	var known_tokens=operators_bin
    }

    for (i=0;i<token_bin.length;i++){
	shape=create_match_shape(possible_matches[i])
	for (j=0;j<canv_state.matches.length;j++){
	    if (shape_equals(shape,canv_state.matches[j])){
		token_bin[i]=1;
		continue;
	    }
	}
    }
    for (key in known_tokens){
	if (array_equals(token_bin,known_tokens[key])){
	    return key;
	}
    }
    return null

    
}
 
//kwa

function read_expression_on_display(canv_state){
    var positions=[1,2,3,5];
    var token_types=['dig','op','dig','dig'];
    var t=[];
    for (var i=0;i<4;i++){
	token=read_digit(positions[i],token_types[i],canv_state);
	t.push(token);
    }
    return t;

}
