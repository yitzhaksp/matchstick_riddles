var match_moved_flg=false

function Shape(x, y, w, h,filled_flg=true,selectable=true,fill= '#AAAAAA') {
  // This is a very simple and unsafe constructor. All we're doing is checking if the values exist.
  // "x || 0" just means "if there is a value for x, use that. Otherwise use 0."
  // But we aren't checking anything else! We could put "Lalala" for the value of x 
  this.x = x || 0;
  this.y = y || 0;
  this.w = w || 1;
    this.h = h || 1;
    this.selectable=selectable
    this.fill=fill
    this.filled_flg=filled_flg
   // alert('creating shape; selctable= '+ this.selectable)
}

// Draws this shape to a given context



Shape.prototype.draw = function(ctx) {
    if (this.filled_flg){
	ctx.fillStyle = this.fill;
	ctx.fillRect(this.x, this.y, this.w, this.h);}
    else {
	ctx.strokeStyle=boundary_color
	ctx.rect(this.x, this.y, this.w, this.h);
	ctx.stroke()}
	
}

// Determine if a point is inside the shape's bounds
Shape.prototype.contains = function(mx, my) {
  // All we have to do is make sure the Mouse X,Y fall in the area between
  // the shape's X and (X + Width) and its Y and (Y + Height)
  return  (this.x <= mx) && (this.x + this.w >= mx) &&
          (this.y <= my) && (this.y + this.h >= my);
}


function CanvasState(canvas) {
  // **** First some setup! ****
  
  this.canvas = canvas;
  this.width = canvas.width;
  this.height = canvas.height;
  this.ctx = canvas.getContext('2d');
  // This complicates things a little but but fixes mouse co-ordinate problems
  // when there's a border or padding. See getMouse for more detail
  var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;
  if (document.defaultView && document.defaultView.getComputedStyle) {
    this.stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingLeft'], 10)      || 0;
    this.stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null)['paddingTop'], 10)       || 0;
    this.styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderLeftWidth'], 10)  || 0;
    this.styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null)['borderTopWidth'], 10)   || 0;
  }
  // Some pages have fixed-position bars (like the stumbleupon bar) at the top or left of the page
  // They will mess up mouse coordinates and this fixes that
  var html = document.body.parentNode;
  this.htmlTop = html.offsetTop;
  this.htmlLeft = html.offsetLeft;

  // **** Keep track of state! ****
  
  this.valid = false; // when set to false, the canvas will redraw everything
    this.matches = [];  // the collection of things to be drawn
  this.match_forms = [];  // the collection of things to be drawn

  this.dragging = false; // Keep track of when we are dragging
  // the current selected object. In the future we could turn this into an array for multiple selection
  this.selection = null;
  this.dragoffx = 0; // See mousedown and mousemove events for explanation
  this.dragoffy = 0;
  
  var myState = this;
  
  //fixes a problem where double clicking causes text to get selected on the canvas
  canvas.addEventListener('selectstart', function(e) { e.preventDefault(); return false; }, false);
  // Up, down, and move are for dragging
  canvas.addEventListener('mousedown', function(e) {
      var mouse = myState.getMouse(e);
      var mx = mouse.x;
      var my = mouse.y;
      var matches = myState.matches;
      var match_forms=myState.match_forms
      var l = matches.length;
      for (var i = l-1; i >= 0; i--) {
          if (matches[i].selectable && matches[i].contains(mx, my)) {
	     // alert('mousedown; selectable='+matches[i].selectable)  
	      var mySel = matches[i];
        // Keep track of where in the object we clicked
        // so we can move it smoothly (see mousemove)
              myState.dragoffx = mx - mySel.x;
              myState.dragoffy = my - mySel.y;
              myState.dragging = true;
              myState.selection = mySel;
              myState.valid = false;
              return;
      }
    }
    // havent returned means we have failed to select anything.
    // If there was an object selected, we deselect it
    if (myState.selection) {
	      myState.valid = false; // Need to clear the old selection border
	      var l = match_forms.length;
      	for (var i = l-1; i >= 0; i--) {
      	    if (match_forms[i].contains(mx, my)) {
      	      //alert('mousedown; selectable='+shapes[i].selectable)  
      		    match_moved_flg=true;
              myState.selection.x=match_forms[i].x;
      		    myState.selection.y=match_forms[i].y;
      		  if (myState.selection.w != match_forms[i].w){
      		    //alert ('equal direction: ' + match_forms[i].direction)
      		    rotate_selection(myState.selection)}
      		    myState.valid = false;
      		    continue;
      	    }
	       } // end for   
	       myState.selection = null;
    } // end outer if
  }, true);
  canvas.addEventListener('mouseup', function(e) {
    myState.dragging = false;
  }, true);
  // double click for making new shapes
  
  // **** Options! ****
  
  this.selectionColor = '#CC0000';
  this.selectionWidth = 2;  
  this.interval = 30;
  setInterval(function() { myState.draw(); }, myState.interval);
}

CanvasState.prototype.addShape = function(shape,shape_type) {
   if (shape_type=='Match'){
       this.matches.push(shape);}
    else {this.match_forms.push(shape)}
  this.valid = false;
}

CanvasState.prototype.clear = function() {
    this.ctx.clearRect(0, 0, this.width, this.height);
    //this.matches=[]
    //this.match_forms=[]
}

CanvasState.prototype.delete_all = function() {
    this.matches=[]
    this.match_forms=[]
}


// While draw is called as often as the INTERVAL variable demands,
// It only ever does something if the canvas gets invalidated by our code
CanvasState.prototype.draw = function() {
  // if our state is invalid, redraw and validate!
  if (!this.valid) {
    var ctx = this.ctx;
      var matches = this.matches;
      var match_forms=this.match_forms;
    this.clear();
    
    // ** Add stuff you want drawn in the background all the time here **
    
    // draw all matches
    var l = matches.length;
    for (var i = 0; i < l; i++) {
      var shape = matches[i];
      // We can skip the drawing of elements that have moved off the screen:
      if (shape.x > this.width || shape.y > this.height ||
          shape.x + shape.w < 0 || shape.y + shape.h < 0) continue;
      shape.draw(ctx);
    }

    // draw all match forms
    var l = match_forms.length;
    for (var i = 0; i < l; i++) {
      var shape = match_forms[i];
      // We can skip the drawing of elements that have moved off the screen:
      if (shape.x > this.width || shape.y > this.height ||
          shape.x + shape.w < 0 || shape.y + shape.h < 0) continue;
      shape.draw(ctx);
    }  
    // draw selection
    // right now this is just a stroke along the edge of the selected Shape
    if (this.selection != null) {
	ctx.strokeStyle = this.selectionColor;
	ctx.lineWidth = this.selectionWidth;
	var mySel = this.selection;
	ctx.strokeRect(mySel.x,mySel.y,mySel.w,mySel.h);
	ctx.strokeStyle=boundary_color
    }
    
    // ** Add stuff you want drawn on top all the time here **
    
    this.valid = true;
  }
}


// Creates an object with x and y defined, set to the mouse position relative to the state's canvas
// If you wanna be super-correct this can be tricky, we have to worry about padding and borders
CanvasState.prototype.getMouse = function(e) {
  var element = this.canvas, offsetX = 0, offsetY = 0, mx, my;
  
  // Compute the total offset
  if (element.offsetParent !== undefined) {
    do {
      offsetX += element.offsetLeft;
      offsetY += element.offsetTop;
    } while ((element = element.offsetParent));
  }

  // Add padding and border style widths to offset
  // Also add the <html> offsets in case there's a position:fixed bar
  offsetX += this.stylePaddingLeft + this.styleBorderLeft + this.htmlLeft;
  offsetY += this.stylePaddingTop + this.styleBorderTop + this.htmlTop;

  mx = e.pageX - offsetX;
  my = e.pageY - offsetY;
  
  // We return a simple javascript object (a hash) with x and y defined
  return {x: mx, y: my};
}


function shape_equals(shape1,shape2){
    if (shape1.x==shape2.x && shape1.y==shape2.y && shape1.w==shape2.w && shape1.h==shape2.h ){
	return true}
    else{
	return false}
}


function rotate_selection(sel){
    w=sel.w
    h=sel.h
    sel.w=h
    sel.h=w
}
