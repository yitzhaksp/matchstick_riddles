function display_expression(expr,canv_state){
    canv_state.delete_all()
    create_digit_background(1,canv_state)
    create_operator_background(canv_state)
    create_digit_background(3,canv_state)
    create_digit_background(5,canv_state)
    create_digit(expr[0],1,canv_state)
    create_operator(expr[1],canv_state)
    create_digit(expr[2],3,canv_state)
    create_equals(canv_state)
    create_digit(expr[3],5,canv_state)
    canv_state.valid=false
    match_moved_flg=false

    //kwa

}

function display_expression_invtoken(expr,canv_state){
    canv_state.delete_all()
    create_digit_background(1,canv_state)
    create_operator_background(canv_state)
    create_digit_background(3,canv_state)
    create_digit_background(5,canv_state)
    create_equals(canv_state)
    create_invexpr(expr,canv_state)
    canv_state.valid=false
    match_moved_flg=false


}

function display_riddle_info(num_matches_to_move){
    document.getElementById('riddle_info').innerHTML = 'Number of Matches to move: '+num_matches_to_move ;
}

function display_difc(difc){
    document.getElementById('difc').innerHTML = 'Difficulty level: '+difc.toString().fontsize(5);


}

function display_riddle_id(riddle_id){
    document.getElementById('riddle_id').innerHTML = 'riddle id: '+riddle_id ;
}

function display_progress_info(num_correct_solutions){
    document.getElementById('progress_info').innerHTML = 'riddles solved: '+num_correct_solutions ;    
}

function display_all(){
   

    if (line_num<riddles.length){
	   display_expression_invtoken(riddles[line_num].expression,s);
	   display_progress_info(num_correct_solutions);
       display_riddle_info(riddles[line_num].matches_to_move);
       display_difc(difcs[riddles[line_num].id])
       if (show_rid_id==true){
        display_riddle_id(riddles[line_num].id)
        //display_riddle_id(112)

       }
       //display_riddle_id(riddles[line_num].id);


    }
    if (line_num==riddles.length){
	document.getElementById('riddles_finished').innerHTML = "You finished all riddles. Click 'to survey' button " ;
	s.delete_all()
	s.valid=false

    }
}

function display_one(rid_id){
      document.getElementById('btn_reset').style.visibility='hidden'
    document.getElementById('btn_check').style.visibility='hidden'
    document.getElementById('btn_next').style.visibility='hidden'
      for (var i = 0; i < riddles.length; i++) {
        if (riddles[i].id==rid_id){
            display_expression_invtoken(riddles[i].expression,s);
        }
      }

}

