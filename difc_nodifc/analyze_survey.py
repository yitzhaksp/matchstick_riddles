import pandas as pd
from format_solution_records.directories import *

survey_questions=['enj','int','perf','sim']
surveys=pd.read_csv(mturk_outp_dir+'all_surveys.csv')
surveys=surveys[surveys['compl']==True]
for col in survey_questions:
    surveys[col]=surveys[col].astype(int)
surveys_difcyes=surveys[surveys['showdifc']==1].copy()
surveys_difcyes.drop('showdifc',axis=1,inplace=True)
surveys_difcno=surveys[surveys['showdifc']==0].copy()
surveys_difcno.drop('showdifc',axis=1,inplace=True)
print(surveys_difcyes.mean().round(2))
print(surveys_difcno.mean().round(2))
surveys_summary=pd.DataFrame()
summary_yes=surveys_difcyes.mean().round(2); summary_yes.name='yes'
summary_yes['amount']=surveys_difcyes.shape[0]
summary_no=surveys_difcno.mean().round(2); summary_no.name='no'
summary_no['amount']=surveys_difcno.shape[0]
surveys_summary=surveys_summary.append(summary_yes)
surveys_summary=surveys_summary.append(summary_no)
surveys_summary.drop('compl',axis=1,inplace=True)
surveys_summary.to_csv(mturk_outp_dir+'surveys_summary_1.csv',index=True,index_label='showdifc')
tmp=7