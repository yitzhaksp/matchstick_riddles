from scipy.stats import ttest_ind
import numpy as np
from difc_nodifc.various_utils import *
from format_solution_records.directories import *
from format_solution_records.various_utils import *
import os
import glob
import pandas as pd
import csv
import json
#os.chdir(records_dir)
verbose=False
extension = 'csv'
all_filenames = [i for i in glob.glob(records_dir+'*.{}'.format(extension))]
coffee_trh=1800
only_easy=True
a=input('does "all_surveys.csv exist and is it up to date ? Type y or n \n')
if a=='y':
    surveys=pd.read_csv(mturk_outp_dir+'all_surveys.csv')
elif a=='n':
    print('run "combine_survey_files.py" in order to generate "all_surveys.csv"')
    print('afterwards run this program again')
    exit()
else:
    print('invalid input')
    exit()

num_seen, num_sol, num_atm= {'sdyes':[], 'sdno':[]}, {'sdyes':[], 'sdno':[]}, {'sdyes':[], 'sdno':[]}
avgsoltime,tottimeonHIT={'sdyes':[],'sdno':[]},{'sdyes':[],'sdno':[]}
for f in all_filenames:
    print('processing '+f)
    df=pd.read_csv(f)
    df.set_index('id',inplace=True)
    df=df[df['required_time']<coffee_trh]
    worker_id=filename_2workerid(f)
    if only_easy:
        df_easy=pd.DataFrame()
        with open('../riddle_difficulty/difcs.json', 'r') as fp:
            difcs=json.load(fp)
        for ind, row in df.iterrows():
            difc=difcs.get(ind,np.nan)
            if (not np.isnan(difc)) and difc<=1:
                df_easy=df_easy.append(row)
        df=df_easy
    if df.shape[0]==0:
        continue
    if df.iloc[0]['showdifc']==True:
        key='sdyes'
    else:
        key='sdno'
    num_seen[key].append(df.shape[0]) #num attempted
    num_atm[key].append(df[df['match_moved'] == True].shape[0])
    num_sol[key].append(df[df['solved']==True].shape[0])
    tottimeonHIT[key].append(df['required_time'].sum())
    if verbose:
        print('num_atm={}'.format(num_seen))
        print('num_mov={}'.format(num_atm))
        print('num_sol={}'.format(num_sol))
    if df[df['solved'] == True].shape[0]>0:
        avgsoltime[key].append( df[df['solved'] == True]['required_time'].sum() / df[df['solved']==True].shape[0])
print('Performance analysis')
if only_easy:
    print('analyzing only easy riddles')
else:
    print('analyzing all riddles')
alpha=0.05
print('one-sided T-test with alpha = {}'.format(alpha))
RejectH0,p=onesid_2samp_ttest(np.array(num_seen['sdyes']), np.array(num_seen['sdno']), alpha)
print('H0 for num_seen (sdyes<=sdno) rejected ? '+ str(RejectH0)+' , p={0:.2f}'.format(p))
RejectH0,p=onesid_2samp_ttest(np.array(num_sol['sdyes']),np.array(num_sol['sdno']),alpha)
print('H0 for num_sol (sdyes<=sdno) rejected ? '+ str(RejectH0)+' , p={0:.2f}'.format(p))
RejectH0,p=onesid_2samp_ttest(np.array(avgsoltime['sdyes']),np.array(avgsoltime['sdno']),alpha)
print('H0 for avgsoltime (sdyes<=sdno) rejected ? '+ str(RejectH0)+' , p={0:.2f}'.format(p))
RejectH0,p=onesid_2samp_ttest(np.array(avgsoltime['sdno']),np.array(avgsoltime['sdyes']),alpha)
print('H0 for avgsoltime (sdno<=sdyes) rejected ? '+ str(RejectH0)+' , p={0:.2f}'.format(p))
RejectH0,p=onesid_2samp_ttest((np.array(num_atm['sdyes']) / np.array(num_seen['sdyes'])), (np.array(num_atm['sdno']) / np.array(num_seen['sdno'])), alpha)
print('H0 for num_atm/num_seen (sdyes<=sdno) rejected ? '+ str(RejectH0)+' , p={0:.2f}'.format(p))
RejectH0,p=onesid_2samp_ttest(np.array(tottimeonHIT['sdyes']),np.array(tottimeonHIT['sdno']),alpha)
print('H0 for tottimeonHIT (sdyes<=sdno) rejected ? '+ str(RejectH0)+' , p={0:.2f}'.format(p))
RejectH0,p=onesid_2samp_ttest(np.array(tottimeonHIT['sdno']),np.array(tottimeonHIT['sdyes']),alpha)
print('H0 for tottimeonHIT (sdno<=sdyes) rejected ? '+ str(RejectH0)+' , p={0:.2f}'.format(p))
survey_questions=['enj','int','perf','sim']
surveys=surveys[surveys['compl']==True]
for col in survey_questions:
    surveys[col]=surveys[col].astype(int)
surveys_difcyes=surveys[surveys['showdifc']==1].copy()
surveys_difcyes.drop('showdifc',axis=1,inplace=True)
surveys_difcno=surveys[surveys['showdifc']==0].copy()
surveys_difcno.drop('showdifc',axis=1,inplace=True)
print('Surveys analysis')
print('num surveys sdyes: {}'.format(surveys_difcyes.shape[0]))
print('num surveys sdno: {}'.format(surveys_difcno.shape[0]))
for atr in survey_questions:
    RejectH0=onesid_2samp_ttest(np.array(surveys_difcyes[atr])  ,   np.array(surveys_difcno[atr]),alpha)
    print('H0 for {} (sdyes<=sdno) rejected ? '.format(atr)+ str(RejectH0))



tmp=7