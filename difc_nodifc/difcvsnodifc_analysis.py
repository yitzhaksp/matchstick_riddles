from format_solution_records.directories import *
from format_solution_records.various_utils import *
import numpy as np
import os
import glob
import pandas as pd
import csv
import json

extension = 'csv'
all_filenames = [x for x in glob.glob(records_dir+'*.{}'.format(extension))]
coffee_trh=1800
rid_cat='hard'
assert(rid_cat=='all'or rid_cat=='easy' or rid_cat=='hard')
num_worksdyes, num_seensdyes, num_solsdyes, num_atmsdyes,totsoltime_sdyes, tottimeonHIT_sdyes= 0, 0, 0, 0, 0,0
num_worksdno, num_seensdno, num_solsdno,num_atmsdno, totsoltime_sdno, tottimeonHIT_sdno= 0, 0, 0, 0, 0,0
if (rid_cat=='easy' or rid_cat=='hard'):
    with open('../riddle_difficulty/difcs.json', 'r') as fp:
        difcs = json.load(fp)
for f in all_filenames:
    print(f)
    df=pd.read_csv(f)
    df.set_index('id',inplace=True)
    df=df[df['required_time']<coffee_trh]
    if rid_cat=='easy':
        df_easy=pd.DataFrame()
        for ind, row in df.iterrows():
            difc=difcs.get(ind,np.nan)
            if (not np.isnan(difc)) and difc<=1:
                df_easy=df_easy.append(row)
        df=df_easy
    if rid_cat=='hard':
        df_hard=pd.DataFrame()
        for ind, row in df.iterrows():
            difc=difcs.get(ind,np.nan)
            if (not np.isnan(difc)) and difc>=3:
                df_hard=df_hard.append(row)
        df=df_hard
    if df.shape[0]==0:
        continue
    worker_id=filename_2workerid(f)
    num_seen=df.shape[0]
    num_solved=df[df['solved']==True].shape[0]
    num_atm = df[df['match_moved'] == True].shape[0]
    wrk_total_timeonHIT=df['required_time'].sum()
    wrk_total_soltime=df[df['solved'] == True]['required_time'].sum()
    if (df.iloc[0]['showdifc']==True):
        num_worksdyes+=1 #sd stands for show difc
        num_seensdyes+=num_seen #num attempted
        num_solsdyes+=num_solved
        num_atmsdyes+=num_atm
        totsoltime_sdyes+=wrk_total_soltime
        tottimeonHIT_sdyes+=wrk_total_timeonHIT
    else:
        num_worksdno += 1
        num_seensdno += num_seen
        num_solsdno += num_solved
        num_atmsdno+=num_atm
        totsoltime_sdno+=wrk_total_soltime
        tottimeonHIT_sdno+=wrk_total_timeonHIT

print('analyzing ' +rid_cat+ ' riddles')
with open('../solution_records/sdyesno_stats1.csv', 'w') as fp:
    mywriter = csv.writer(fp, delimiter=',',lineterminator='\n')
    mywriter.writerow(['attribute','yes','no'])
    mywriter.writerow(['num_workers',num_worksdyes,num_worksdno])
    mywriter.writerow(['num_seen', num_seensdyes, num_seensdno])
    mywriter.writerow(['avg_seen', round(num_seensdyes / num_worksdyes, 2), round(num_seensdno / num_worksdno, 2)])
    mywriter.writerow(['avg_solved',round(num_solsdyes/num_worksdyes,2), round(num_solsdno/num_worksdno,2)])
    mywriter.writerow(['avg_attempted',round(num_atmsdyes/num_worksdyes,2), round(num_atmsdno/num_worksdno,2)])
    mywriter.writerow(['avg_solratio', round(num_solsdyes / num_seensdyes, 2), round(num_solsdno / num_seensdno, 2)])
    mywriter.writerow(['avg_atmratio', round(num_atmsdyes / num_seensdyes, 2), round(num_atmsdno / num_seensdno, 2)])
    mywriter.writerow(['avg_timeonHIT',round(tottimeonHIT_sdyes/num_worksdyes,2), round(tottimeonHIT_sdno/num_worksdno,2)])
    mywriter.writerow(['avg_soltime',round(totsoltime_sdyes/num_solsdyes,2), round(totsoltime_sdno/num_solsdno,2)])





tmp=7