from scipy.stats import ttest_ind

#H0: mean(X)<=mean(Y)
def onesid_2samp_ttest(X,Y,alpha):
    xm,ym=X.mean(),Y.mean()
    if X.mean()<=Y.mean():
        Reject='no'
        p=1.0
    else:
        p = ttest_ind(X, Y).pvalue
        #print('p :{}'.format(p))
        if p/2 < alpha:
            Reject='yes'
        else:
            Reject='no'
    return Reject,p