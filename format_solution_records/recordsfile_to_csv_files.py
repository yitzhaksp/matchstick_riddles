import pandas as pd
from format_solution_records.formatting_utils import *
from format_solution_records.directories import *
import os
import numpy as np
import csv

# first delete all rows where there are no solutions from raw records file
if not os.path.exists(records_dir):
    os.makedirs(records_dir)
if not os.path.exists(surveys_dir):
    os.makedirs(surveys_dir)
rawrecs=pd.read_csv(mturk_outp_dir + 'Batch_3may_difcno.csv')
rawrecs=rawrecs.dropna(subset=['Answer.solutions_data','Answer.survey_data'])
rawrecs['Answer.survey_data']=rawrecs['Answer.survey_data'].astype('string')
showdifc = input("Was difclev shown to the worker? Enter 0 or 1:\n")
assert(showdifc=='0' or showdifc=='1')
num_files=0
for ind,row in rawrecs.iterrows():
    worker_id=row['WorkerId']
    worker_string=row['Answer.worker_data']
    recs_string=row['Answer.solutions_data']
    print(ind)
    recs_list=recs_string.split('&');
    recs_list=recs_list[1:]
    rec_dicts_list=[]
    for rec_string in recs_list:
        rec_dict=record_to_dict(rec_string)
        rec_dict['showdifc']=showdifc
        rec_dicts_list.append(rec_dict)
    recs_df=pd.DataFrame(rec_dicts_list)
    thisworker_recfile_name=records_dir+'records_'+worker_id+'.csv'
    if not os.path.exists(thisworker_recfile_name):
        num_files+=1
        print('creating records file')
        recs_df.to_csv(thisworker_recfile_name,index=False)
        print('creating survey file')
        with open(surveys_dir + 'worker_' + worker_id + '.txt', 'w') as fp:
            fp.write(row['Answer.survey_data']+'_$showdifc='+showdifc)
    else:
        print('worker '+worker_id +' already has a records file')
    worker_list=worker_string.split('$');
    worker_list=worker_list[1:]
    worker_dict=record_to_dict(worker_string)
    if not os.path.exists(workers_dir):
        os.makedirs(workers_dir)
    with open(workers_dir+'worker_'+worker_id+'.txt','w') as fp:
        fp.write('id: ' + worker_id + '\n')
        for key in worker_dict:
            fp.write(key+': '+worker_dict[key]+'\n')
print('num files created: {}'.format(num_files))
tmp=7
