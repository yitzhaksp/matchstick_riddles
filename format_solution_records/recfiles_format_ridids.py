import numpy as np
import pandas as pd
from format_solution_records.directories import *
from format_solution_records.various_utils import *
import os
import glob
if not os.path.exists(tmp_dir):
    os.mkdir(tmp_dir)
all_filenames = [x.split('/')[4] for x in glob.glob(records_dir+'*.{}'.format('csv'))]
for f in all_filenames:
    df = pd.read_csv(records_dir+f)
    df['id']=df['id'].astype(str)
    df.drop(columns=['skipped'],inplace=True)
    for ind, row in df.iterrows():
        df.at[ind,'id']='r'+df.at[ind,'id']
    df.to_csv(tmp_dir+f,index=False)

tmp=1
