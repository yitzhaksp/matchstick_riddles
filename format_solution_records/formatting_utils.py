import re

def surveystr_2dict(surveystr):
    a=surveystr.split('_$')
    a=a[1:]
    survey_dict = {}
    for b in a:
        c = b.split('=')
        survey_dict[c[0]] = c[1]
    return survey_dict

def get_feature_names(record):
    record_features_asstrings = record.split('$')
    record_features_asstrings = record_features_asstrings[1:]
    feature_names=[]
    for feature_string in record_features_asstrings:
        feature_names.append(feature_string.split('=')[0])
    return feature_names

def record_to_dict(record):
    record_features_asstrings=record.split('$')
    record_features_asstrings=record_features_asstrings[1:]
    record_dict={}
    for feature_string in record_features_asstrings:
        tmp1=feature_string.split('=')
        feature_name=tmp1[0]
        feature_value=re.sub('_', '',tmp1[1])
        #print('featname: '+feature_name)
        record_dict[feature_name]=feature_value
    return record_dict