import numpy as np
import pandas as pd
import glob
from format_solution_records.directories import *

def compute_sigma(times_dict,coffee_trh):
    sum_sigmas = 0
    for id in times_dict.keys():
        clean_times1 = [x for x in times_dict[id] if (x < coffee_trh)]
        sum_sigmas += np.std(clean_times1)
    avg_sigma = sum_sigmas / len(times_dict)
    return avg_sigma
#attention: in order to merge needs to be called twice
def merge_recdirs_partial(dir1,dir2,already_processed,outp_dir):
    dir1_filenames = [f.split('/')[3] for f in glob.glob(dir1+'*.csv')]
    dir2_filenames = [f.split('/')[3] for f in glob.glob(dir2+'*.csv')]
    for f in dir1_filenames:
        print('checking '+f)
        if not (f in already_processed):
            df1=pd.read_csv(dir1+f)
            if f in dir2_filenames:
                print('merging')
                df2 = pd.read_csv(dir1 + f)
                df=pd.concat([df1,df2])
            else:
                df=df1
            df.to_csv(outp_dir + f, index=False, encoding='utf-8-sig')
            already_processed.append(f)
    return already_processed

def filename_2workerid(f):
    a=f.split('_')[1]
    b=a.split('.')[0]
    return b

def remove_outliers(data,remove_trh,clip_trh,sigma,num_sigmas):
    data=data[data<remove_trh]
    #data=np.clip(data,0,clip_trh)
    ind=data-np.mean(data) < num_sigmas*sigma
    ab=1
    return data[ind]

def get_num_correct_from_rec(rec):
    rec_list=rec.split('$')
    a=rec_list[1]
    b=a.split('ct_')
    c=b[1]
    d=c.split('__')
    num_correct=int(d[0])
    return num_correct

