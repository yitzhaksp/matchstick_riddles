from format_solution_records.directories import *
from format_solution_records.various_utils import *
from format_solution_records.formatting_utils import *
import os
import glob
import pandas as pd
#set working directory
os.chdir(surveys_dir)
extension = 'txt'
all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
df=pd.DataFrame()
for f in all_filenames:
    with open(f, 'r') as fp:
        survey_str = fp.read().splitlines()[0]
    worker_id=f.split('_')[1].split('.')[0]
    x=pd.Series(surveystr_2dict(survey_str))
    x.name=worker_id
    df=df.append(x)
    #df['worker_id']=filename_2workerid(f)

df.to_csv( "../all_surveys.csv", index=True, index_label='worker_id')