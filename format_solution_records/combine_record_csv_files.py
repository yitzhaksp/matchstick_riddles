from format_solution_records.directories import *
from format_solution_records.various_utils import *

import os
import glob
import pandas as pd
#set working directory
os.chdir(records_dir)

#find all csv files in the folder
#use glob pattern matching -> extension = 'csv'
#save result in list -> all_filenames
extension = 'csv'
all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
#print(all_filenames)

#combine all files in the list
df_list=[]
for f in all_filenames:
    df=pd.read_csv(f)
    worker_id=filename_2workerid(f)
    df['worker_id']=worker_id
    df_list.append(df)
    #print ('w_id '+ worker_id)

combined_csv = pd.concat(df_list)
combined_csv.to_csv( "../all_records.csv", index=False, encoding='utf-8-sig')