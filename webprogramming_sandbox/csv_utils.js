var line_num=0;
var lines=[];

function handleFile(file) {
	// Check for the various File API support.
    console.log(file);
    if (window.FileReader) {
		// FileReader are supported.
		getAsText(file);
	} else {
		alert('FileReader are not supported in this browser.');
	}
}

function getAsText(fileToRead) {
	var reader = new FileReader();
	// Handle errors load
	reader.onload = loadHandler;
	reader.onerror = errorHandler;
	// Read file into memory as UTF-8      
	reader.readAsText(fileToRead);
}

function loadHandler(event) {
    console.log('execute loadHandler');
    var csv = event.target.result;
	csv_obj_to_array(csv);             
}

function csv_obj_to_array(csv) {
    var allTextLines = csv.split(/\r\n|\n/);
    //var lines = [];
    while (allTextLines.length) {
        lines.push(allTextLines.shift().split(','));
    }
	console.log(lines);
}


function errorHandler(evt) {
	if(evt.target.error.name == "NotReadableError") {
		alert("Can't read file !");
	}
}

function next_line_to_console(){
    console.log('next num is:');
    console.log(lines[line_num]);
    line_num=line_num+1;

}
    
