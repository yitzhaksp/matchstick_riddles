import pandas as pd
from format_solution_records.formatting_utils import *
from format_solution_records.directories import *
from format_solution_records.various_utils import *
import os
import numpy as np
import csv

bonus_per_solution=0.1
suf='3may_difcno'
out_file_name=mturk_outp_dir+'bonuses_'+suf+'.csv'
recs_strings=pd.read_csv(mturk_outp_dir+'Batch_'+suf+'.csv')
# first delete all rows where there are no solutions from raw records file
recs_strings=recs_strings.dropna(subset=['Answer.solutions_data'])

out_recs=[]
with open(out_file_name, 'w') as fp:
    mywriter = csv.writer(fp, delimiter=',', lineterminator='\n')
    mywriter.writerow(['worker_id','assignment_id','bonus'])
    for ind,row in recs_strings.iterrows():
        print(ind)
        worker_id=row['WorkerId']
        assignment_id=row['AssignmentId']
        num_correct=get_num_correct_from_rec(row['Answer.solutions_data'])
        out_rec=worker_id+","+str(num_correct)
        out_recs.append(out_rec)
        mywriter.writerow([worker_id,assignment_id, str(num_correct*bonus_per_solution)])

tmp=7



